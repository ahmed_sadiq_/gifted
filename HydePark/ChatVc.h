//
//  ChatVc.h
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "Alert.h"
#import "HomeVC.h"
#import "WDUploadProgressView.h"
#import "AppDelegate.h"
#import "WDUploadProgressView.h"
#import "ASIFormDataRequest.h"
#import "autocompleteHandle.h"
#import "ChatThread.h"
#import "Followings.h"
#import "PopularUsersModel.h"
@interface ChatVc : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayoutConstrain;
@property (strong, nonatomic) IBOutlet ChatThread *chatThread;
@property (weak, nonatomic) IBOutlet PopularUsersModel *userModl;
@property (weak, nonatomic) IBOutlet Followings *following;
@property (weak, nonatomic) IBOutlet UserChannelModel *userChannel;

@property (weak, nonatomic) IBOutlet UIButton *btnSend;

@end
