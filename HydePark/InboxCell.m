//
//  InboxCell.m
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "InboxCell.h"

@implementation InboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.userImg.layer.masksToBounds = YES;
    
    self.userImg.layer.cornerRadius = 70.0 / 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
