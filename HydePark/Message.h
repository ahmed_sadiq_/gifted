//
//  Message.h
//  TalentTube
//
//  Created by Samreen Noor on 30/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
@property (nonatomic, retain) NSString *message_id;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *sent_by;
@property (nonatomic, retain) NSString *user_name;
@property (nonatomic, retain) NSString *profile_link;
@property (nonatomic, retain) NSString *date_time;
@end
