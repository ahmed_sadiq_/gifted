//
//  MessageCell.m
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.msgView.layer.masksToBounds = YES;
    
    self.msgView.layer.cornerRadius = 5.0;
    self.userImg.layer.masksToBounds = YES;
    
    self.userImg.layer.cornerRadius = 65.0/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
