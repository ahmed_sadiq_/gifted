//
//  DataContainer.h
//  HydePark
//
//  Created by TxLabz on 30/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "myChannelModel.h"
@interface DataContainer : NSObject
{
    NSMutableArray *notificationsArray;
    NSMutableArray *myAchiveArray;
    NSMutableArray *whoToFollow;
    NSMutableArray *followings;
    NSMutableArray *followers;
}
@property int searchPageNum;
@property int forumPageNumber;
@property int pageNum;
@property int myCornerPageNum;
@property (strong, nonatomic) myChannelModel *_profile;
@property (strong, nonatomic) NSMutableArray *notificationsArray;
@property (strong, nonatomic) NSMutableArray *myArchiveArray;
@property (strong, nonatomic) NSMutableArray *whoToFollow;
@property (strong, nonatomic) NSMutableArray *followings;
@property (strong, nonatomic) NSMutableArray *followers;
@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) NSMutableArray *forumsVideo;
@property (strong, nonatomic) NSMutableArray *channelVideos;
@property (strong, nonatomic) NSMutableArray *newsfeedsVideos;
@property (strong, nonatomic) NSMutableArray *uploadedChannlVideos;
@property (strong, nonatomic) NSMutableArray *friendsArray;
@property (nonatomic) BOOL moveFromMenu;

@property (strong, nonatomic) NSString *chatFriendId;
@property (nonatomic)         BOOL            isChaVC;
@property (nonatomic)         BOOL            isInboxVc;

@property (nonatomic)         BOOL            famousUsersFetchedFirst;
@property (nonatomic)         BOOL            notifcationsFetcedFirst;
@property (nonatomic)         int             whoToFollowPageNum;
@property (nonatomic)         int             notificationsPageNum;
@property (nonatomic)         int             whoToFollowPreviousCount;
@property (nonatomic)         int             notificationsPreviousCount;
@property (nonatomic)         int             unreadMsgCount;
@property (nonatomic)         int             commentPageNum;

+ (id)sharedManager;
- (void)dellocDate;
@end
