//
//  InboxCell.h
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastmsg;
@property (weak, nonatomic) IBOutlet UIButton *btnInbox;

@end
