//
//  ChatThread.h
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatThread : NSObject
@property (nonatomic, retain) NSString *thread_id;
@property (nonatomic, retain) NSString *friend_id;
@property (nonatomic, retain) NSString *friend_name;
@property (nonatomic, retain) NSString *profile_link;
@property (nonatomic, strong) NSString *last_message;
@property (nonatomic, retain) NSString *date_time;

@end
