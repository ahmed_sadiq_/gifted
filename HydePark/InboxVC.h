//
//  InboxVC.h
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "Alert.h"
#import "HomeVC.h"
#import "WDUploadProgressView.h"
@interface InboxVC : UIViewController<UITableViewDelegate>
{
    NSMutableArray *inBoxArray;
    UIActivityIndicatorView *activityIndicator;
    NSUInteger currentSelectedIndex;
    NSMutableArray *userList;

}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTopConstrain;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;
@end
