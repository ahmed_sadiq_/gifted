//
//  DrawerVC.m
//  HydePark
//
//  Created by Mr on 22/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "DrawerVC.h"
#import "Utils.h"
#import "HomeVC.h"
#import "MyBeam.h"
#import "Topics.h"
#import "NavigationHandler.h"
#import "ProfileVC.h"
#import "Constants.h"
#import "DataContainer.h"
@interface DrawerVC ()

@end

static DrawerVC *DrawerVC_Instance= NULL;

@implementation DrawerVC

@synthesize _currentState;
float _yLocation;

+(DrawerVC *)getInstance{
    
    if(DrawerVC_Instance == NULL)
    {
        if (IS_IPAD){
            DrawerVC_Instance = [[DrawerVC alloc] initWithNibName:@"DrawerVC_iPad" bundle:nil];
            _yLocation = 0.0f;
        }
        
        else{
            DrawerVC_Instance = [[DrawerVC alloc] initWithNibName:@"DrawerVC" bundle:nil];
            _yLocation = 0.0f;
        }
    }
    return DrawerVC_Instance;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        _currentState = OFF_HIDDEN;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
   
   // [self getUnreedMessagesCount];


}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = [[UIApplication sharedApplication] delegate];
   // [self getUnreedMessagesCount];
    [self updatedCount];
    if (IS_IPHONE_6 ) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 667);
    }else if (IS_IPHONE_5){
    
    }
    else if (IS_IPHONE_6Plus){
        
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 736)];
    }
    DrawerVC_Instance = self;
    removeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [removeBtn setFrame:CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height)];
    [removeBtn addTarget:self action:@selector(ShowInView) forControlEvents:UIControlEventTouchUpInside];
}
-(void)updatedCount{
    
    
    DataContainer *shareMngr = [DataContainer sharedManager];
    if(shareMngr.unreadMsgCount>0){
        _lblCount.hidden = NO;
        _countImg.hidden = NO;
        _lblCount.text = [NSString stringWithFormat:@"%d",shareMngr.unreadMsgCount];
    }
    else{
        _lblCount.hidden = YES;
        _countImg.hidden = YES;
    }
    if (shareMngr.notificationsPreviousCount>0) {
        _btnNofiCount.hidden=NO;
        NSString *notfCount = [NSString stringWithFormat:@"%d",shareMngr.notificationsPreviousCount];
        [_btnNofiCount setTitle:notfCount forState:UIControlStateNormal];
    }
    else{
     _btnNofiCount.hidden=YES;
    }

}

-(void)AddInView:(UIView *)_parentView{
    parentView = _parentView;
    
    //if(_currentState == OFF_HIDDEN)
        //        [self.view setFrame:CGRectMake(parentView.frame.size.width, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
        if (IS_IPAD) {
            [self.view setFrame:CGRectMake(-424, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
        }else{
            
            [self.view setFrame:CGRectMake(-280, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            if (IS_IPHONE_6) {
                [self.view setFrame:CGRectMake(-298, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    [_parentView addSubview:self.view];

}

-(void)ShowInView{
    [self updatedCount];
    if(_currentState == OFF_HIDDEN)
    {
        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            _currentState = ON_SCREEN;
            
            // final view elements attributes goes here
            // [self.view setFrame:CGRectMake(parentView.frame.size.width - self.view.frame.size.width, _yLocation, self.view.frame.size.width, parentView.frame.size.height-_yLocation)];
            [self.view setFrame:CGRectMake(0, _yLocation, self.view.frame.size.width, parentView.frame.size.height-_yLocation)];
            
            [parentView addSubview:removeBtn];
            [parentView bringSubviewToFront:self.view];
            
            
        } completion:nil];
    }
    
    else if(_currentState == ON_SCREEN)
    {
        [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            _currentState = OFF_HIDDEN;
            //[[parentView viewWithTag:HIDE_TAG] removeFromSuperview];
            
            // final view elements attributes goes here
            // [self.view setFrame:CGRectMake(parentView.frame.size.width, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            if (IS_IPAD) {
                [self.view setFrame:CGRectMake(-424, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            }else{
                [self.view setFrame:CGRectMake(-280, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
                if (IS_IPHONE_6) {
                    [self.view setFrame:CGRectMake(-298 , _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
                }
            }
            [removeBtn removeFromSuperview];
            
        } completion:nil];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)generalAction:(id)sender{
    
    [self ShowInView];
}


- (IBAction)Home:(id)sender {
  
    [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        _currentState = OFF_HIDDEN;

        if (IS_IPAD) {
            [self.view setFrame:CGRectMake(-424, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
        }else{
            [self.view setFrame:CGRectMake(-280, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            if (IS_IPHONE_6) {
                [self.view setFrame:CGRectMake(-298 , _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
        [removeBtn removeFromSuperview];
        
    } completion:nil];

    
    
    [[NavigationHandler getInstance]PopToRootViewController];
   // [[NavigationHandler getInstance]NavigateToHomeScreen];


}

- (IBAction)MyBeam:(id)sender {
  
   [[NavigationHandler getInstance]MoveToMyBeam];
    
}
-(IBAction)BeamFromGallery:(id)sender{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"UploadFromGallery"
     object:self];
}
- (IBAction)Messages:(id)sender {
    
}

- (IBAction)Notification:(id)sender {
 
    [[NavigationHandler getInstance]MoveToNotifications];
}

- (IBAction)Profile:(id)sender {
    
    [[NavigationHandler getInstance]MoveToProfile];
   
}

- (IBAction)Topics:(id)sender {
    [self ShowInView];
    [[NavigationHandler getInstance]MoveToTopics];
}

- (IBAction)Settings:(id)sender {
}

- (IBAction)Logout:(id)sender {
      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profileImg"];
    [[NavigationHandler getInstance]LogoutUser];
}

- (IBAction)WhotoFollow:(id)sender {
   
    [[NavigationHandler getInstance]MoveToPopularUsers];
}
- (IBAction)btnInbox:(id)sender {
    [[NavigationHandler getInstance]MoveToInbox];
}


-(void)getUnreedMessagesCount{

    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_UNREAD_MSG_COUNT,@"method",
                              token,@"Session_token", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                //////Comments Videos Response //////
                int unreadCount = [[result objectForKey:@"unseen_count"] intValue];
                int unreadNotifCount = [[result objectForKey:@"unseen_notifications_count"] intValue];

                if (unreadCount>0) {
                    _lblCount.hidden = NO;
                    _countImg.hidden = NO;
                    _lblCount.text = [NSString stringWithFormat:@"%d",unreadCount];
                  DataContainer *shareMng   = [DataContainer sharedManager];
                    shareMng.unreadMsgCount = unreadCount;
                    shareMng.notificationsPreviousCount = unreadNotifCount;

                }
                if (unreadNotifCount>0) {
                    _btnNofiCount.hidden = NO;
                    
                    NSString *unreadNotCount = [NSString stringWithFormat:@"%d",unreadNotifCount];
                    [_btnNofiCount setTitle: unreadNotCount forState: UIControlStateNormal];

                    DataContainer *shareMng   = [DataContainer sharedManager];
                    shareMng.notificationsPreviousCount = unreadNotifCount;
                    
                }
                
                               }
                
                
                       }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Network Problem. Try Again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }];
    
    
    

}



@end
