//
//  TopicsCell.h
//  TalentTube
//
//  Created by Samreen Noor on 31/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBeamName;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end
