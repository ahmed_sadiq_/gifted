//
//  InboxVC.m
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "InboxVC.h"
#import "Constants.h"
#import "InboxCell.h"
#import "ChatThread.h"
#import "AppDelegate.h"
#import "AVFoundation/AVFoundation.h"
#import "VideoModel.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WDUploadProgressView.h"
#import "Alert.h"
#import "ASIFormDataRequest.h"
#import "autocompleteHandle.h"
#import "UIImageView+WebCache.h"
#import "ChatVc.h"
#import "DataContainer.h"
#import "DrawerVC.h"
@implementation InboxVC
- (void)viewDidLoad {
    [super viewDidLoad];
    inBoxArray=[[NSMutableArray alloc]init];
    userList=[[NSMutableArray alloc]init];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    _searchBar.autocorrectionType = UITextAutocorrectionTypeYes;

    [self getChatThreads];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addNewChat" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addNewChat:)
                                                 name:@"addNewChat"
                                               object:nil];
    [[DataContainer sharedManager] setIsInboxVc:true];
}
-(void)viewWillAppear{
    
    [super viewWillAppear:YES];
    
    
    

}

-(void) addNewChat:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"addNewChat"])
    {
        NSDictionary* userInfo = notification.object;
       ChatThread  *chatT= userInfo[@"newChat"];
        int temp=0;
        for (int i=0;i< inBoxArray.count;i++) {
            ChatThread  *t=inBoxArray[i];
            if ([t.friend_id isEqualToString:chatT.friend_id]) {
                [inBoxArray replaceObjectAtIndex:i withObject:chatT];
                temp=1;
                break;
            }
        }
        if (temp==0) {
            [inBoxArray insertObject:chatT atIndex:0];
            [userList insertObject:chatT atIndex:0];
        }
        
        [_tblView reloadData];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [[DataContainer sharedManager] setIsInboxVc:false];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD)
        return 110;
    else
        return 87;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  inBoxArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    InboxCell *cell;
    
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InboxCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InboxCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

       ChatThread *chatTd = [[ChatThread alloc] init];
    chatTd = [inBoxArray objectAtIndex:indexPath.row];
    
    cell.userName.text = chatTd.friend_name;
    cell.lblLastmsg.text = chatTd.last_message;
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString:chatTd.profile_link] placeholderImage:[UIImage imageNamed:@"userplace"]];
    [cell.btnInbox addTarget:self action:@selector(inboxPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnInbox setTag:indexPath.row];
       [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)inboxPressed:(UIButton *)sender{
    
    UIButton *statusBtn = (UIButton *)sender;
   currentSelectedIndex = statusBtn.tag;
    ChatThread *chatTd = [[ChatThread alloc] init];
    chatTd = [inBoxArray objectAtIndex:currentSelectedIndex];
    ChatVc *frndChat = [[ChatVc alloc] initWithNibName:@"ChatVc" bundle:nil];
    frndChat.chatThread = chatTd;

    [[self navigationController] pushViewController:frndChat animated:YES];

}
-(void) getChatThreads{

    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_CHAT_THREAD,@"method",
                              token,@"Session_token", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        { [activityIndicator stopAnimating];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                //////Comments Videos Response //////
               NSArray *inbox = [result objectForKey:@"chat_threads"];
            
                
                for(NSDictionary *tempDict in inbox){
                    
                    ChatThread *chatT = [[ChatThread alloc] init];
                    
                    chatT.thread_id = [tempDict objectForKey:@"thread_id"];
                    chatT.friend_id = [tempDict objectForKey:@"friend_id"];
                    chatT.friend_name = [tempDict objectForKey:@"friend_name"];
                    chatT.last_message = [tempDict objectForKey:@"last_message"];
                    chatT.date_time = [tempDict objectForKey:@"date_time"];
                    chatT.profile_link = [tempDict objectForKey:@"profile_link"];
                    
                    [inBoxArray addObject:chatT];
                    [userList addObject:chatT];
                }
                
                
            
                [_tblView reloadData];
                ////////////////////////////////
              
                
                ///////////////////////////////// move tabl
            }
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        else{
            [activityIndicator stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Network Problem. Try Again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }];


}

- (IBAction)back:(id)sender {
   // [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] updatedCount];
    
   [self.navigationController popViewControllerAnimated:YES];
}
#pragma  mark - searchBar Methods


- (IBAction)btnSearch:(id)sender {
    
    
   // CGRect frame = _tbView.frame;
   // float tblY = frame.origin.y;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if (_searchTopConstrain.constant >= 0) {
            [_searchTopConstrain setConstant:-44];
            
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 
                                 [self.view layoutIfNeeded];
                                 
                             }];
            _searchBar.hidden=YES;
            [_searchBar setAlpha:0.0];
            _searchBar.text = @"";
            [_searchBar resignFirstResponder];
            
            [_searchBar setShowsCancelButton:NO animated:YES];
            
            inBoxArray = userList;
            
            [_tblView reloadData];
           // [_searchBar resignFirstResponder];

            
        }
        else{
            [_searchTopConstrain setConstant:0];
            
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 
                                 [self.view layoutIfNeeded];
                                 
                             }];
            _searchBar.hidden=NO;
            
            
            [_searchBar setAlpha:1.0];
             [_searchBar becomeFirstResponder];

        }
        
    }
     ];
 
    
}



-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    NSLog(@"searchBarTextDidBeginEditing");
    
    [searchBar setShowsCancelButton:YES animated:YES];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];

    [_searchTopConstrain setConstant:-44];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    inBoxArray = userList;
    
    [_tblView reloadData];
}
/////////////////////////////////
- (void)getActiveUserListForSearch

{
    // NSString *name;
    NSMutableArray *searchArray=[[NSMutableArray alloc]init];
    
    if (_searchBar.text) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.friend_name CONTAINS[cd] %@",_searchBar.text];
        searchArray = [userList filteredArrayUsingPredicate:predicate].mutableCopy;
        
        //[searchArray addObject:dic];
        
    }
    
    
    if (searchArray.count>0) {
        
        inBoxArray = searchArray;
        
        [_tblView reloadData];
    }
    
    
}
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    [self performSelector:@selector(getActiveUserListForSearch) withObject:nil afterDelay:0.15];
    
    return YES;
}


@end
