//
//  ChatVc.m
//  TalentTube
//
//  Created by Samreen Noor on 29/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ChatVc.h"
#import "Constants.h"
#import "InboxCell.h"
#import "MessageCell.h"
#import "Message.h"
#import "UIImageView+WebCache.h"
#import "DataContainer.h"
@interface ChatVc ()
{
    NSString *textToshare;
    float keyboardSize;
    NSMutableArray *msgArray;
    UIActivityIndicatorView *activityIndicator;
    NSString *msgTxt;
    NSString *userName;
    NSString *userProfile;
    DataContainer *sharedManager;
}
@end

@implementation ChatVc
-(void) setUpView{
 

  userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserName"];
    userProfile = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileImg"];
    
    [self setupNotificationObserver];
    _lblUserName.text = _chatThread.friend_name;
    [[DataContainer sharedManager] setChatFriendId:_chatThread.friend_id];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.tblView addGestureRecognizer:tap];
    


}
-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addNewMsg" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addNewMsg:)
                                                 name:@"addNewMsg"
                                               object:nil];

}
-(void) addNewMsg:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"addNewMsg"])
    {
        NSDictionary* userInfo = notification.object;
        Message  *mObj= userInfo[@"newMsg"];
        [msgArray addObject:mObj];
        [_tblView reloadData];
        NSInteger numberOfRows = [_tblView numberOfRowsInSection:0];//on send msg show last table cell index..
        if (numberOfRows) {
            [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }

    }
}

-(void)viewDidDisappear:(BOOL)animated{

    [[DataContainer sharedManager] setIsChaVC:false];
    [[DataContainer sharedManager] setChatFriendId:nil];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    sharedManager = [DataContainer sharedManager];
    [sharedManager setIsChaVC:true];
    [self setUpView];
    msgArray =[[NSMutableArray alloc]init];
    if (![_chatThread.thread_id isEqualToString:@"0"]) {
        [self getchat];

    }
}
-(void)dismissKeyboard {
    [_tfMessage resignFirstResponder];
    
    [_tfMessage setText:@""];
    
    
    _bottomLayoutConstrain.constant = 0;
}

#pragma mark - View Controller Methods

- (void)setupNotificationObserver
{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    keyboardSize=keyboardFrame.size.height;
    
    NSLog(@"keyboardFrame: %f", keyboardSize);
    
    
    //[_contrainInput setConstant:keyboardSize+5.0];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
}


#pragma mark - Keyboard Methods

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect frameEnd;
    NSDictionary *userInfo = [notification userInfo];
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&frameEnd];
    //[self moveTableView];
    self.bottomLayoutConstrain.constant = frameEnd.size.height;
    [UIView animateWithDuration:.3 animations:^{
        [self.tfMessage layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:animationDuration delay:0.3 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
    } completion:^(BOOL finished) {
        [self moveTableView];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSTimeInterval animationDuration;
    NSDictionary *userInfo = [notification userInfo];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    self.bottomLayoutConstrain.constant = 0;
    [UIView animateWithDuration:.3 animations:^{
        [self.tfMessage layoutIfNeeded];
    }];
    
    
    [UIView animateWithDuration:animationDuration delay:0.3 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        _tblView.contentInset = contentInsets;
        _tblView.scrollIndicatorInsets = contentInsets;
        
    } completion:^(BOOL finished) {
       
        
            [self moveTableView];
    }];
    
}
-(void) moveTableView{
    
    
    // if (commentsObj.CommentsArray.count >4) {
    
    NSInteger numberOfRows = [_tblView numberOfRowsInSection:0];//on send msg show last table cell ..
    if (numberOfRows) {
        [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        // }
    }
}

-(void) moveToBottom{
    
    
      if (msgArray.count >5) {
    
    [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:msgArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
     }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD)
        return 110;
    else if (IS_IPHONE_5){
    
        Message *tempmsg = [[Message alloc]init];
        
        tempmsg = [msgArray objectAtIndex:indexPath.row];
        CGSize size = [tempmsg.message sizeWithFont:[UIFont fontWithName:@"Helvetica" size:15] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        NSLog(@"%f",size.height);

        if (size.height <13) {
            return size.height+80;
            
        }
        if (size.height <20) {
            return size.height+100;
        }
        else if (size.height <50){
            return size.height+120;
            
            
        }
        else if (size.height <80){
            return size.height+150;
            
        }
        else if (size.height <100){
            return size.height+180;
            
        }
        else if (size.height <130){
            
            return size.height+230;
            
        }
        else
            
            return size.height+320;
        
        

    }
    else
    {  Message *tempmsg = [[Message alloc]init];
        
       tempmsg = [msgArray objectAtIndex:indexPath.row];
        CGSize size = [tempmsg.message sizeWithFont:[UIFont fontWithName:@"Helvetica" size:15] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        NSLog(@"%f",size.height);
        if (size.height <20) {
          return size.height+80;
        }
        return size.height+108;}


}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  msgArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MessageCell *cell;
    Message *chat = [[Message alloc] init];
    chat = [msgArray objectAtIndex:indexPath.row];
    if ([chat.sent_by isEqualToString:@"USER"]) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MessageLeft" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    

    cell.lblDateTime.text = chat.date_time;
    cell.lblMsg.text = chat.message;
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString:chat.profile_link] placeholderImage:[UIImage imageNamed:@"userplace"]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void) getchat{
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_CHAT,@"method",
                              token,@"Session_token",@"1",@"page_no",_chatThread.thread_id,@"thread_id", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        { [activityIndicator stopAnimating];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                //////Comments Videos Response //////
                sharedManager.unreadMsgCount = [[result objectForKey:@"unseen_count"] intValue];
                sharedManager.notificationsPreviousCount = [[result objectForKey:@"unseen_notifications_count"]intValue];
              NSArray  *chatArray = [result objectForKey:@"chat_messages"];
             
                
                for(NSDictionary *tempDict in chatArray){
                    
                    Message *msg = [[Message alloc] init];
                    
                    msg.message_id = [tempDict objectForKey:@"message_id"];
                    msg.message = [tempDict objectForKey:@"message"];
                    msg.sent_by = [tempDict objectForKey:@"sent_by"];
                    msg.user_name = [tempDict objectForKey:@"user_name"];
                    msg.date_time = [tempDict objectForKey:@"date_time"];
                    msg.profile_link = [tempDict objectForKey:@"profile_link"];
                    
                    [msgArray addObject:msg];
                }
                
                
                
                [_tblView reloadData];
                ////////////////////////////////
                NSInteger numberOfRows = [_tblView numberOfRowsInSection:0];//on send msg show last table cell index..
                if (numberOfRows) {
                    [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                }
                
                ///////////////////////////////// move tabl
            }
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        else{
            [activityIndicator stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Network Problem. Try Again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }];

    
    


}
-(void) sendMsgCall:(NSString *)msg{

    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEND_MESSAGE,@"method",
                              token,@"session_token",_chatThread.friend_id,@"friend_id",msg,@"message",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            NSString *thread = [result objectForKey:@"thread_id"];

            if(success == 1){
                _chatThread.thread_id = thread;
                [_following setThreadId:thread];
                [_userModl  setThreadId:thread];
                [_userChannel  setThreadId:thread];

                if ([message isEqualToString:@"Message Sent Successfully."]) {

                    
                }
            }
            else{
            
            }
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
-(void) showAlert{
  Alert  *alert = [[Alert alloc] initWithTitle:@"Post has been uploaded" duration:(float)2.0f completion:^{
        //
    }];
    [alert setDelegate:self];
    [alert setShowStatusBar:YES];
    [alert setAlertType:AlertTypeSuccess];
    [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
    [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
    [alert setBounces:YES];
    [alert showAlert];
}

//    NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:chatArray.count-1 inSection:0];
//    // Add them in an index path array
//    NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
//    // Launch reload for the two index path
//    [self.collectionView reloadItemsAtIndexPaths:indexArray];
-(void) addLocalMessage:(NSString *)msg{
    
    
    
    Message *m = [[Message alloc] init];
    [m setMessage:_tfMessage.text];
    [m setSent_by:@"USER"];
    [m setUser_name:userName];
   [m setProfile_link:userProfile];
    [m setDate_time:[self getLocalDateTime]];
    [msgArray addObject:m];
    [_tblView reloadData];
    _tfMessage.text=@"";

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSend:(id)sender {
    msgTxt = _tfMessage.text;
    [_tfMessage resignFirstResponder];
    [self addLocalMessage:_tfMessage.text];
    [self sendMsgCall:msgTxt];

}
#pragma mark - helper


-(NSString *)getLocalDateTime{
    
        NSDateFormatter *dtFormat = [[NSDateFormatter alloc] init];
        NSDate *today = [NSDate date];
        
        [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
        return [dtFormat stringFromDate:today];
    }
#pragma mark - txfield delegates
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@""]) {
        _btnSend.enabled = NO;
    }
    else{
        _btnSend.enabled = YES;

    }

    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
